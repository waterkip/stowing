# SPDX-FileCopyrightText: 2023 Wesley Schwengle
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM debian:testing-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN <<OEF
apt-get update
apt-get upgrade -y
apt-get install --no-install-recommends -y \
    ca-certificates \
    stow \
    vim-nox \
    vim-tiny \
    zsh
apt-get autoremove -y
apt-get clean -y

groupadd -g 1000 stow
useradd -ms /bin/zsh -u 1000 -g 1000 stow -d /home/stow
OEF

USER stow
WORKDIR /home/stow
